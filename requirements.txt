Django==3.2.2
django-ninja==0.12.3
django-extensions==3.1.3
python-decouple==3.4
django-localflavor==3.0.1
pytest-django==4.2.0
djoser==2.1.0
django-cors-headers==3.7.0
