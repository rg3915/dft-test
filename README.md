# DFT Test

Read [challenge](challenge.md)

## This project was done with:

* [Python 3.8.9](https://www.python.org/)
* [Django 3.2.2](https://www.djangoproject.com/)
* [Django Ninja](https://www.django-rest-framework.org/)
* [VueJS 2.6.11](https://vuejs.org/)


## How to run project?

* Clone this repository.
* Create virtualenv with Python 3.
* Active the virtualenv.
* Install dependences.
* Run the migrations.


```
python manage.py drf_create_token admin
```

## Docs swagger

http://localhost:8000/api/v1/docs

```
{
    "shop_id":1,
    "product_id":1,
    "quantity":2,
    "price":120
}
```